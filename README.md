# Static Webpage with Zola - Individual Project1

This repository contains a static webpage created using Zola, a fast static site generator. The webpage is hosted on GitLab Pages.

Link to Webpage : https://ayush-gupta96-ag758-dd3c570f98f861f58ac2b749955ccef9c9b218cbf16.gitlab.io/

Link to Video : https://www.youtube.com/watch?v=SR8F8hUhePw

## Getting Started

To view the webpage locally or make modifications, follow these steps:

1. **Clone the Repository**: 
   ```
   git clone <repository_url>
   ```

2. **Install Zola**: 
   Follow the [official installation instructions](https://www.getzola.org/documentation/getting-started/installation/) to install Zola on your system.

3. **Navigate to the Project Directory**: 
   ```
   cd <project_directory>
   ```

4. **Build the Site**: 
   ```
   zola build
   ```

5. **View the Site Locally**: 
   After building the site, you can view it locally by running:
   ```
   zola serve
   ```
   Open your web browser and go to `http://127.0.0.1:1111`.

## Contributing

If you'd like to contribute to this project, you can do so by following these steps:

1. Fork this repository.

2. Create a new branch:
   ```
   git checkout -b feature/new-feature
   ```

3. Make your changes and commit them:
   ```
   git commit -am 'Add new feature'
   ```

4. Push to the branch:
   ```
   git push origin feature/new-feature
   ```

5. Create a new Pull Request.

## Gitlab Workflow 

Our website deployment process is seamlessly managed through GitLab Pages, offering an automated and efficient workflow. Whenever changes are pushed to the main branch of our repository, GitLab Pages springs into action, initiating a series of processes to rebuild and update our webpage, ensuring that the latest changes are reflected live.

GitLab Pages leverages the power of GitLab's robust infrastructure to swiftly rebuild our website, incorporating any modifications or additions made to the main branch. This automation not only saves time but also minimizes the risk of human error, ensuring that our website remains up-to-date and consistent with our development efforts.

By utilizing GitLab Pages for deployment, we embrace a DevOps approach, where development and operations seamlessly intersect to deliver value to our users. This integration fosters collaboration among team members, accelerates the pace of development, and enhances the overall efficiency of our workflow.

## Hosting / Deployment: 

The website is hosted on GitLab, providing a reliable and convenient platform for deployment. Leveraging GitLab's hosting capabilities ensures seamless integration with our development workflow, as it allows for direct deployment from our Git repository. With GitLab hosting, we benefit from features such as GitLab Pages, which automates the deployment process upon pushing changes to the designated branch, in our case, the main branch. 

This setup not only simplifies deployment but also ensures that our website remains accessible and up-to-date for our users. Additionally, GitLab's robust infrastructure offers scalability and reliability, guaranteeing consistent performance and uptime for our hosted website. Overall, hosting on GitLab provides us with a streamlined deployment solution, empowering our team to focus on delivering high-quality content and features to our audience.

## Resources

- [Zola Documentation](https://www.getzola.org/documentation/getting-started/installation/)
- [GitLab Pages Documentation](https://docs.gitlab.com/ee/user/project/pages/)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
