+++
title = "Nutri AI (Generative AI Hackathon by Open AI)"

[extra]
image = "../nutri2.png"
link = "https://github.com/ayushg245/Nutri-AI---Generative-AI-Hackathon/blob/main/png/ExampleImage0%20.png"
technologies = ["Generative AI", "Python", "Prompt Engineering", "GitHub"]
+++
Nutri AI app recommends dishes based on user prompts with nutrition facts, budget, and recipe details. Future plans include custom recipes matching groceries, dietary needs, time, and budget, alongside suggestions for nearby supermarkets, streamlining meal planning for healthier choices.

