+++
title = "Stock Price Analysis Application"

[extra]
image = "../StockPriceAnalysis.png"
link = "https://github.com/ayushg245/Nutri-AI---Generative-AI-Hackathon/blob/main/png/ExampleImage0%20.png"
technologies = ["Databricks,", "Spark SQL", "Delta Lake", "Docker"]
+++
Developed a stock price analysis application by using PySpark for Data Wrangling, Spark SQL for data querying, ETL operations, and Delta Lake for reliable and scalable data storage.
Implemented Infrastructure as Code through Dockerization, Azure ACR deployment, and automated data pipelines, with load tests.