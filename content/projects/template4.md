+++
title = "Prediction of Airbnb Pricing in Asheville, NC:"

[extra]
image = "../airbnb.png"
link = ""
technologies = ["R", "Regression Modelling", "Teamwork,", "Problem Solving"]
+++
Developed a pricing prototype for Airbnb listings in Asheville, NC using regression modeling in R-Studio. My relevant skills include proficiency in R, regression modeling, teamwork, communication skills, and problem-solving abilities.